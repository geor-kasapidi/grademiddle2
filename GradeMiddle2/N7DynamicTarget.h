//
//  N7DynamicTarget.h
//  GradeMiddle2
//
//  Created by Георгий Касапиди on 31.03.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "N7DynamicMethod.h"

@interface N7DynamicTarget : NSObject

@property (strong, nonatomic) NSNumber *demoProperty;

@property (strong, nonatomic) N7DynamicMethod *dynamicMethod;

@end
