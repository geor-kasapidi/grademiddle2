//
//  AppDelegate.m
//  GradeMiddle2
//
//  Created by Георгий Касапиди on 31.03.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "AppDelegate.h"

#import "N7Array.h"
#import "N7DynamicStuff.h"
#import "N7SubtractionStuff.h"
#import "N7MultiplicationStuff.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    N7DynamicStuff *x = [N7DynamicStuff new];
    
    x.evenTarget = [N7SubtractionStuff new];
    x.oddTarget = [N7MultiplicationStuff new];
    
    NSLog(@"%@", @([x calculateStuff:5]));
    NSLog(@"%@", @([x calculateStuff:2]));
    
    return YES;
}

@end
