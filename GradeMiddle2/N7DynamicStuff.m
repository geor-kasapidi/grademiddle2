//
//  N7DynamicStuff.m
//  GradeMiddle2
//
//  Created by Георгий Касапиди on 31.03.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7DynamicStuff.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wprotocol"

@implementation N7DynamicStuff

- (NSMethodSignature *)methodSignatureForSelector:(SEL)aSelector
{
    if ([NSStringFromSelector(aSelector) isEqualToString:NSStringFromSelector(@selector(calculateStuff:))]) {
        NSString *types = [NSString stringWithFormat:@"%s%s%s%s", @encode(NSInteger), @encode(id), @encode(SEL), @encode(NSInteger)];
        
        return [NSMethodSignature signatureWithObjCTypes:[types UTF8String]];
    }
    
    return [super methodSignatureForSelector:aSelector];
}

- (void)forwardInvocation:(NSInvocation *)anInvocation
{
    NSInteger argValue;
    
    [anInvocation getArgument:&argValue atIndex:2];
    
    [anInvocation invokeWithTarget:argValue % 2 == 0 ? self.evenTarget : self.oddTarget];
}

@end

#pragma clang diagnostic pop
