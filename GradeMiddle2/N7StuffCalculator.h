//
//  N7StuffCalculator.h
//  GradeMiddle2
//
//  Created by Георгий Касапиди on 31.03.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol N7StuffCalculator <NSObject>

- (NSInteger)calculateStuff:(NSInteger)someNumber;

@end
