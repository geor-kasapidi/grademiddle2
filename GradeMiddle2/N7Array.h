//
//  N7Array.h
//  GradeMiddle2
//
//  Created by Георгий Касапиди on 31.03.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface N7Array<T> : NSArray

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithObjects:(const T [])objects count:(NSUInteger)cnt NS_UNAVAILABLE;
- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;

- (instancetype)initWithItems:(nullable T)firstItem, ... NS_REQUIRES_NIL_TERMINATION NS_DESIGNATED_INITIALIZER;

- (nonnull T)objectAtIndex:(NSUInteger)index;

@end

NS_ASSUME_NONNULL_END
