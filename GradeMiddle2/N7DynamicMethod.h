//
//  N7DynamicMethod.h
//  GradeMiddle2
//
//  Created by Георгий Касапиди on 31.03.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface N7DynamicMethod : NSObject

@property (strong, nonatomic) NSNumber *demoProperty;

@end
