//
//  N7DynamicTarget.m
//  GradeMiddle2
//
//  Created by Георгий Касапиди on 31.03.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7DynamicTarget.h"

@implementation N7DynamicTarget

@dynamic demoProperty;

- (N7DynamicMethod *)dynamicMethod
{
    if (!_dynamicMethod) {
        _dynamicMethod = [N7DynamicMethod new];
    }
    
    return _dynamicMethod;
}

- (id)forwardingTargetForSelector:(SEL)aSelector
{
    NSString *selString = NSStringFromSelector(aSelector);
    
    if ([selString isEqualToString:NSStringFromSelector(@selector(setDemoProperty:))] ||
        [selString isEqualToString:NSStringFromSelector(@selector(demoProperty))]) {
        return self.dynamicMethod;
    }
    
    return [super forwardingTargetForSelector:aSelector];
}

@end
