//
//  N7DynamicStuff.h
//  GradeMiddle2
//
//  Created by Георгий Касапиди on 31.03.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "N7StuffCalculator.h"

@interface N7DynamicStuff : NSObject <N7StuffCalculator>

@property (strong, nonatomic) id<N7StuffCalculator> evenTarget;
@property (strong, nonatomic) id<N7StuffCalculator> oddTarget;

@end
