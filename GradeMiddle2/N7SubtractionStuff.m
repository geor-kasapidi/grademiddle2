//
//  N7SubtractionStuff.m
//  GradeMiddle2
//
//  Created by Георгий Касапиди on 31.03.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7SubtractionStuff.h"

@implementation N7SubtractionStuff

- (NSInteger)calculateStuff:(NSInteger)someNumber
{
    return someNumber - 10;
}

@end
