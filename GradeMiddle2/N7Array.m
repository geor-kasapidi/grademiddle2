//
//  N7Array.m
//  GradeMiddle2
//
//  Created by Георгий Касапиди on 31.03.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7Array.h"

struct node
{
    void *value;
    struct node *prev;
};

@implementation N7Array
{
    struct node *_tail;
    NSUInteger _count;
}

- (instancetype)initWithItems:(id)firstItem, ...
{
    if (self = [super init]) {
        va_list args;
        va_start(args, firstItem);
        
        struct node *tail = NULL;
        
        NSUInteger count = 0;
        
        for (id arg = firstItem; arg != nil; arg = va_arg(args, id)) {
            struct node *node = malloc(sizeof(struct node));
            
            node->value = (__bridge void *)arg;
            node->prev = tail;
            
            tail = node;
            
            count++;
        }
        
        _tail = tail;
        _count = count;
        
        va_end(args);
    }
    
    return self;
}

- (void)dealloc
{
    struct node *node = _tail;
    
    while (node != nil) {
        void *temp = node;
        
        node = node->prev;
        
        free(temp);
    }
    
    free(node);
    
    _tail = NULL;
}

#pragma mark -

- (NSUInteger)count
{
    return _count;
}

- (id)objectAtIndex:(NSUInteger)index
{
    NSUInteger n = _count;
    
    if (index < n) {
        struct node *node = _tail;
        
        for (NSUInteger i = 0; i < n - index - 1; i++) {
            node = node->prev;
        }
        
        return (__bridge id)node->value;
    }
    
    return nil;
}

@end
