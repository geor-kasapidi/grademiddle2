//
//  N7MultiplicationStuff.m
//  GradeMiddle2
//
//  Created by Георгий Касапиди on 31.03.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7MultiplicationStuff.h"

@implementation N7MultiplicationStuff

- (NSInteger)calculateStuff:(NSInteger)someNumber
{
    return someNumber * 2;
}

@end
