//
//  N7DynamicMethod.m
//  GradeMiddle2
//
//  Created by Георгий Касапиди on 31.03.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import <objc/runtime.h>

#import "N7DynamicMethod.h"

@implementation N7DynamicMethod
{
    NSNumber *_internalVariable;
}

@dynamic demoProperty;

+ (BOOL)resolveInstanceMethod:(SEL)sel
{
    NSString *selString = NSStringFromSelector(sel);
    
    if ([selString isEqualToString:NSStringFromSelector(@selector(setDemoProperty:))]) {
        NSString *types = [NSString stringWithFormat:@"%s%s%s%s", @encode(void), @encode(id), @encode(SEL), @encode(id)];
        
        class_addMethod([self class], sel, (IMP)demoSetter, [types UTF8String]);
        
        return YES;
    } else if ([selString isEqualToString:NSStringFromSelector(@selector(demoProperty))]) {
        NSString *types = [NSString stringWithFormat:@"%s%s%s", @encode(id), @encode(id), @encode(SEL)];
        
        class_addMethod([self class], sel, (IMP)demoGetter, [types UTF8String]);
        
        return YES;
    }
    
    return [super resolveInstanceMethod:sel];
}

id demoGetter(id self, SEL _cmd)
{
    return [self valueForKey:@"internalVariable"];
}

void demoSetter(id self, SEL _cmd, id value)
{
    [self setValue:value forKey:@"internalVariable"];
}

@end
