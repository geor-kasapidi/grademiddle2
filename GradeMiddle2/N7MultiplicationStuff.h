//
//  N7MultiplicationStuff.h
//  GradeMiddle2
//
//  Created by Георгий Касапиди on 31.03.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "N7StuffCalculator.h"

@interface N7MultiplicationStuff : NSObject <N7StuffCalculator>

@end
